conkyrc

Je vous présente mon fichier de configuration pour l'utilisation de Conky. Plus d'infos : http://monptitnuage.fr/index.php?article142/utilisation-de-conky-un-tableau-de-bord-interactif
Détail des informations affichées :

    TABLEAU DE BORD.
    Date.
    Nom d'utilisateur actuellement connecté.
    Version du kernel utilisé.
    Temps depuis que l'ordinateur est allumé.


* RESUME.
Nombres de processus présent sur la machine.
Nombres de processus en cours.
Utilisation de la RAM + barre de progression.
Utilisation du Swap + barre de progression.

* CPU.
Utilisation de tous les coeurs de la machine + barre de progression en couleur rouge.

* RESEAU.
Addresse IP Wifi.
Adresse IP Ethernet.
Adresse IP Publique.

* ESPACE DISQUE.
Utilisation de l'espace disque pour Root en barre de progression.
Utilisation de l'espace disque pour Home en barre de progression.

* UTILISATION CPU.
Détail de l'utilisation du CPU.

* UTILISATION MEMOIRE.
Détail de l'utilisation de la mémoire.
=====================================================================================

Concernant l'affichage de l'adresse IP Publique, la configuration dans le fichier est un peu plus compliqué. Je vous met ci-dessous la procédure trouvé à cette adresse https://blog.pastoutafait.fr/billets/Conky-Exemple-de-configuration-sous-Ubuntu :

Un petit script doit être utilisé pour aller chercher votre IP. Voilà un exemple très simple de script, copiez le code suivant dans un fichier nommé par exemple "ip.sh" :

#!/bin/bash
wget http://checkip.dyndns.org/ -O - -o /dev/null | cut -d: -f 2 | cut -d< -f 1

Il faut rendre ce fichier "ip.sh" exécutable :

$ sudo chmod 755 ip.sh

Maintenant il faut indiquer dans votre fichier de configuration conky qu'il doit exécuter ce script et afficher le résultat avec une ligne comme celle-ci :

IP Publique : ${execi 1800 /home/utilisateur/ip.sh}